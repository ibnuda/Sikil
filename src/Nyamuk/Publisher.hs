{-# LANGUAGE OverloadedStrings #-}
module Nyamuk.Publisher where

import           Control.Concurrent
import           Control.Concurrent.STM
import           Data.Text
import           Data.Text.Encoding     as DT
import           Network.MQTT

publishMessage :: Text -> IO ()
publishMessage load = do
  commands <- mkCommands
  pub <- newTChanIO
  let mqtt =
        (defaultConfig commands pub) {cUsername = Just "iotuser", cPassword = Just "open please"}
  _ <- forkIO $ do
    -- publish mqtt Handshake False "dari/comment.hs" (BS.pack load)
    publish mqtt Handshake False "dari/comment.hs" $ DT.encodeUtf8 load
    disconnect mqtt
  term <- run mqtt
  case term of
    _ -> return ()
