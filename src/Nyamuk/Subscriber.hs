{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Nyamuk.Subscriber where

import Control.Monad
import Control.Concurrent
import Control.Concurrent.STM
import System.Exit
import Network.MQTT

errything :: Topic
errything = "#"

handleMessage :: Message 'PUBLISH -> IO ()
handleMessage mess =
  unless (retain $ header mess) $ do
  let t = topic $ body mess
      p = payload $ body mess
  -- insert to db here
  putStrLn $ show t ++ " and " ++ show p

subscribeEverything :: IO ()
subscribeEverything = do
  commands <- mkCommands
  pubs <- newTChanIO
  let mqtt = (defaultConfig commands pubs) {cUsername = Just "iotuser", cPassword = Just "open please"}
  _ <- forkIO $ do
    qosG <- subscribe mqtt [(errything, Handshake)]
    case qosG of
      [Handshake] -> forever $ atomically (readTChan pubs) >>= handleMessage
      _ -> exitFailure
  thing <- run mqtt
  case thing of
    _ -> return ()
