{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}

module Foundation where

import           Database.Persist.Sql (ConnectionPool, runSqlPool)
import           Import.NoFoundation
import           Text.Hamlet          (hamletFile)
import           Text.Jasmine         (minifym)

import           Yesod.Auth.Account
import           Yesod.Auth.Dummy

import qualified Data.CaseInsensitive as CI
import qualified Data.Text.Encoding   as TE
import           Yesod.Core.Types     (Logger)
import qualified Yesod.Core.Unsafe    as Unsafe
import           Yesod.Default.Util   (addStaticContentExternal)

data App = App
  { appSettings    :: AppSettings
  , appStatic      :: Static -- ^ Settings for static file serving.
  , appConnPool    :: ConnectionPool -- ^ Database connection pool.
  , appHttpManager :: Manager
  , appLogger      :: Logger
  }

data MenuItem = MenuItem
  { menuItemLabel          :: Text
  , menuItemRoute          :: Route App
  , menuItemAccessCallback :: Bool
  }

data MenuTypes
  = NavbarLeft MenuItem
  | NavbarRight MenuItem

mkYesodData "App" $(parseRoutesFile "config/routes")

type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

instance Yesod App where
  approot =
    ApprootRequest $ \app req ->
      case appRoot $ appSettings app of
        Nothing   -> getApprootText guessApproot app req
        Just root -> root
  makeSessionBackend _ =
    Just <$>
    defaultClientSessionBackend
      120 -- timeout in minutes
      "config/client_session_key.aes"
  yesodMiddleware = defaultYesodMiddleware
  defaultLayout widget = do
    master <- getYesod
    mmsg <- getMessage
    muser <- maybeAuthId
    mcurrentRoute <- getCurrentRoute
    (title, parents) <- breadcrumbs
    let menuItems =
          [ NavbarLeft $
            MenuItem {menuItemLabel = "Home", menuItemRoute = HomeR, menuItemAccessCallback = True}
          , NavbarLeft $
            MenuItem
            { menuItemLabel = "Profile"
            , menuItemRoute = ProfileR
            , menuItemAccessCallback = isJust muser
            }
          , NavbarRight $
            MenuItem
            { menuItemLabel = "Login"
            , menuItemRoute = AuthR LoginR
            , menuItemAccessCallback = isNothing muser
            }
          , NavbarRight $
            MenuItem
            { menuItemLabel = "Logout"
            , menuItemRoute = AuthR LogoutR
            , menuItemAccessCallback = isJust muser
            }
          ]
    let navbarLeftMenuItems = [x | NavbarLeft x <- menuItems]
    let navbarRightMenuItems = [x | NavbarRight x <- menuItems]
    let navbarLeftFilteredMenuItems = [x | x <- navbarLeftMenuItems, menuItemAccessCallback x]
    let navbarRightFilteredMenuItems = [x | x <- navbarRightMenuItems, menuItemAccessCallback x]
    pc <-
      widgetToPageContent $ do
        addStylesheet $ StaticR css_bootstrap_css
        $(widgetFile "default-layout")
    withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")
  authRoute _ = Just $ AuthR LoginR
  isAuthorized (AuthR _) _   = return Authorized
  isAuthorized CommentR _    = return Authorized
  isAuthorized HomeR _       = return Authorized
  isAuthorized FaviconR _    = return Authorized
  isAuthorized RobotsR _     = return Authorized
  isAuthorized (StaticR _) _ = return Authorized
  isAuthorized ProfileR _    = isAuthenticated
  addStaticContent ext mime content = do
    master <- getYesod
    let staticDir = appStaticDir $ appSettings master
    addStaticContentExternal
      minifym
      genFileName
      staticDir
      (StaticR . flip StaticRoute [])
      ext
      mime
      content
    where
      genFileName lbs = "autogen-" ++ base64md5 lbs
  shouldLog app _source level =
    appShouldLogAll (appSettings app) || level == LevelWarn || level == LevelError
  makeLogger = return . appLogger

instance YesodBreadcrumbs App where
  breadcrumb HomeR     = return ("Home", Nothing)
  breadcrumb (AuthR _) = return ("Login", Just HomeR)
  breadcrumb ProfileR  = return ("Profile", Just HomeR)
  breadcrumb  _        = return ("home", Nothing)

instance YesodPersist App where
  type YesodPersistBackend App = SqlBackend
  runDB action = do
    master <- getYesod
    runSqlPool action $ appConnPool master
instance YesodPersistRunner App where
  getDBRunner = defaultGetDBRunner appConnPool

instance PersistUserCredentials User where
  userUsernameF = UserUsername
  userPasswordHashF = UserPassword
  userEmailF = UserEmailAddress
  userEmailVerifiedF = UserVerified
  userEmailVerifyKeyF = UserVerifyKey
  userResetPwdKeyF = UserResetPasswordKey
  uniqueUsername = UniqueUsername
  userCreate name email key pwd = User name pwd email False key ""

instance YesodAuth App where
  type AuthId App = Username
  getAuthId = return . Just . credsIdent
  loginDest _ = HomeR
  logoutDest _ = HomeR
  authPlugins _ = [accountPlugin]
  authHttpManager _ = error "No manager needed"
  onLogin = return ()
  maybeAuthId = lookupSession credsKey

instance AccountSendEmail App

isAuthenticated :: Handler AuthResult
isAuthenticated = do
  muid <- maybeAuthId
  return $
    case muid of
      Nothing -> Unauthorized "You must login to access this page"
      Just _  -> Authorized

instance YesodAuthAccount (AccountPersistDB App User) App where
  runAccountDB = runAccountPersistDB

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

instance HasHttpManager App where
  getHttpManager = appHttpManager

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger
