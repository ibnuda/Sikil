{-# LANGUAGE OverloadedStrings #-}
module Handler.Comment where

import           Control.Concurrent
import           Import
import           Nyamuk.Publisher

postCommentR :: Handler Value
postCommentR = do
  comment <- (requireJsonBody :: Handler Comment)
  -- maybeCurrentUserId <- maybeAuthId
  _ <- liftIO $ forkIO $ do
    publishMessage $ commentMessage comment
  -- let comment' = comment {commentUserId = maybeCurrentUserId}
  -- insertedComment <- runDB $ insertEntity comment'
  -- returnJson insertedComment
  returnJson comment
